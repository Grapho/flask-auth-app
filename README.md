# Пример приложения с готовой системой авторизации пользователей.

Данное приложение выполнено для демонстрации возможностей модуля авторизации Flask.

## Запуск приложения

Для запуска приложения достаточно его склонировать к себе в рабочую директорию:
```bash
cd ~/workspace/
git clone http://10.0.21.22:7990/scm/fast/flask_auth_app.git
```

После клонирования репозитория достаточно открыть директорию ~/flask_auth_app в VSCode и выполнить запуск приложения (нажать F5).

Если VSCode не установлен или запуск необходимо выполнить без использования VSCode, то достаточно выполнить в консоли следующие команды:
```
cd ~/workspace/flask_auth_app/
export FLASK_APP=project
export FLASK_DEBUG=1
flask run
```

В обоих случаях, если приложение запустилось корректно в браузере можно открыть графический интерфейс приложения http://localhost:5000/.

----

## Зависимости

Зависимости проекта прописаны в файле <a href="requirements.txt">requirements.txt</a>.

