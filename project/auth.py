from flask import (
    Blueprint,
    render_template,
    redirect,
    url_for,
    request,
    flash,
)

from flask_login import (
    login_user,
    logout_user,
    login_required,
)

from werkzeug.security import (
    generate_password_hash,
    check_password_hash,
)

from project.models import User

from . import db

auth = Blueprint('auth', __name__)


@auth.route('/signup_post', methods=["POST"])
def signup_post():
    email = request.form.get("email")
    username = request.form.get("username")
    password = request.form.get("password")

    user = User.query.filter_by(email=email).first()
    if user:
        flash(f"Пользователь с электронной почтой [{email}] уже существует!")
        return redirect(url_for("auth.signup"))

    password = generate_password_hash(password=password, method="sha256")

    db.session.add(User(
        email=email,
        username=username,
        password=password
    ))
    db.session.commit()
    return redirect(url_for("auth.login"))


@auth.route('/signup')
def signup():
    return render_template('signup.html')


@auth.route('/login_post', methods=["POST"])
def login_post():
    email = request.form.get("email")
    password = request.form.get("password")
    remember = True if request.form.get("remember") else False

    user = User.query.filter_by(email=email).first()
    if not user or not check_password_hash(pwhash=user.password, password=password):
        flash("Авторизация не удалась! Проверьте корректность введенных данных!")
        return redirect(url_for("auth.login"))
    login_user(user=user, remember=remember)
    return redirect(url_for("main.profile"))


@auth.route('/login')
def login():
    return render_template('login.html')


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for("auth.login"))
